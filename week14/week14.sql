use company;

create temporary table TopSuppliers(
	SupplierID int primary key,
    SupplierName varchar(45),
    ProductCount int 
);

insert into TopSuppliers(SupplierID, SupplierName, ProductCount)
select s.SupplierID, SupplierName, count(ProductID) as ProductCount
from suppliers s join products p on s.SupplierID = p.SupplierID
group by s.SupplierID
order by ProductCount desc; 

select * from TopSuppliers where ProductCount > 3;

call check_table_exists("TopSuppliers");
drop temporary table TopSuppliers;

select * from customers;

update customers
set CustomerName="Emre Coskun", city="Mugla", PostalCode="48000", Country="Turkey"
where CustomerID=1;

delete from customers
where CustomerID=2;

truncate table Customers;
delete from Customers;
# same things but more efficient one is truncate table
# truncate resets the auto increment
# no where statemnet for truncate

drop table Customers;

